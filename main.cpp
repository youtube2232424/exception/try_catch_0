/**
* This program calculates the area of rectangle using CMD
* version 1.1. 17.06.2024
*/

#include <iostream>			//standart input / output library
#include <stdexcept>		//for exception

using namespace std;

/*
* To calculate area of rectangles. Throw exception for negative arguments.
* @param length The length. Must be positive.
* @param  width The width. Must be positive
* @return An area of rectangle.
*/
double area(const double length, const double width);

int main(void)
{
	char ch; 
	double length;
	double width;

	while (true)			//infinite loop for input
	{
		cout << "Enter two numbers for area or 'q' for exit: ";

		cin >> ch;		// for check 'q'
		if (tolower(ch) == 'q')
			break;

		cin.putback(ch);

		cin >> length;
		cin >> width;

		try
		{
			cout << "An area: " << area(length, width) << '\n';
		}
		catch (runtime_error& e)
		{
			cerr << "Runtime error area(): " << e.what() << '\n';
		}
	}

	return 0;
}

double area(const double length, const double width)
{
	if (length < 0 || width < 0)
		throw runtime_error(string("Negative argument "));

	return length * width;
}